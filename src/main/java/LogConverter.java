import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LogConverter {

    public static Object[][] JSONRead(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object[][] objectNodes = mapper.readValue(json, Object[][].class);
        return objectNodes;
    }

    public static int[] easyAnalyzeAndTransform(List<Session> sessionList) {
        int total = 0;
        int totalZerosWhenEnemyHasPositiveScore = 0;
        int meAndEnemyZeros = 0;
        for (Session session : sessionList) {
            total += session.myScore;
            if (session.myScore == 0 && session.enemyScore > 0) {
                totalZerosWhenEnemyHasPositiveScore++;
            }
            if (session.myScore == 0 && session.enemyScore == 0) {
                meAndEnemyZeros++;
            }
        }
        int[] results = new int[5];

        System.out.println("(easyAnalyzeAndTransform) TOTAL SCORE IS : " + total);
        results[0] = total;
        System.out.println("(easyAnalyzeAndTransform) TOTAL ZEROS WHEN ENEMYS SCORE IS >0 : " + totalZerosWhenEnemyHasPositiveScore);
        results[1] = totalZerosWhenEnemyHasPositiveScore;
        System.out.println("(easyAnalyzeAndTransform) AGREEMENTS NOT FOUND : " + meAndEnemyZeros);
        results[2] = meAndEnemyZeros;
        System.out.println("(easyAnalyzeAndTransform) TOTAL AGREEMENTS WITHOUT ZEROS " + (sessionList.size() - (totalZerosWhenEnemyHasPositiveScore + meAndEnemyZeros)));
        results[3] = (sessionList.size() - (totalZerosWhenEnemyHasPositiveScore + meAndEnemyZeros));
        System.out.println("(easyAnalyzeAndTransform) TOTAL ZEROS percent " + ((totalZerosWhenEnemyHasPositiveScore + meAndEnemyZeros) * 100) / sessionList.size());
        results[4] = ((totalZerosWhenEnemyHasPositiveScore + meAndEnemyZeros) * 100) / sessionList.size();
        return results;
    }

    public static List<Session> parseFile(String path) throws IOException {
        //String path = "D:\\Projects\\bps_insurance\\log-parser\\remoteLog.json";
        BufferedReader reader = new BufferedReader(new FileReader(path));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        Object[][] objects = JSONRead(builder.toString());
        List<Session> map = map(objects);
        reader.close();
        return map;
    }

    private static List<Session> map(Object[][] arr) {
        if (arr == null) {
            throw new NullPointerException("Передан пустой массив");
        }
        List<Session> sessions = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            Session session = new Session();
            for (int j = 0; j < arr[i].length; j++) {
                mapByType((List<Object>) arr[i][j], session);
            }
            sessions.add(session);
        }
        return sessions;
    }

    private static void mapByType(List<Object> list, Session session) {
        Object o = list.get(0);
        Class<?> aClass = o.getClass();
        if (String.class == aClass) {
            String str = (String) o;
            switch (str) {
                case "init":
                    session.first = orderCheck(list.get(1));
                    break;
                case "offer":
                    session.rounds += checkOfferCount(list.get(1), session.first);
                    break;
                case "info":
                    break;
                case "abort":
                    session.aborted = Boolean.TRUE;
                    break;
                case "score":
                    checkOffer(list, session);
                    break;
                default:
                    break;
            }
        }
    }

    private static void checkOffer(List<Object> list, Session session) {
        Object o = list.get(1);
        Object score = list.get(2);
        Class<?> aClass = o.getClass();
        if (Integer.class == aClass && score.getClass() == Integer.class) {
            if (session.first == 0 && (Integer) o == 0) {
                session.myScore = (Integer) score;
            }
            if (session.first == 1 && (Integer) o == 0) {
                session.enemyScore = (Integer) score;
            }
            if (session.first == 0 && (Integer) o == 1) {
                session.enemyScore = (Integer) score;
            }
            if (session.first == 1 && (Integer) o == 1) {
                session.myScore = (Integer) score;
            }
        } else {
            throw new IllegalArgumentException("еверный формат score");
        }
    }

    private static int checkOfferCount(Object o, int order) {
        Class<?> aClass = o.getClass();
        if (Integer.class == aClass) {
            Integer result = (Integer) o;
            if (order == 0) {
                return result & 0;
            } else {
                return result & 1;
            }
        } else {
            throw new IllegalArgumentException("еверный формат offer");
        }

    }

    private static int orderCheck(Object o) {
        List<Object> list = (List<Object>) o;
        Object o1 = list.get(0);
        Class<?> aClass = o1.getClass();
        if (String.class == aClass) {
            String string = (String) o1;
            if (string.equals("remote")) {
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new IllegalArgumentException("еверный формат init");
        }
    }

    public static class Session {

        public int first;

        public int myScore;

        public int enemyScore;

        public int rounds;

        public boolean aborted;

        public Session() {

        }
    }
}