import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    private static int COLUMN_WIDTH = 40;
    private static String FORMAT_BEGINING = "%";
    private static String FORMAT_ENDING = "-40s%n";
    private static String FORMAT_MIDDLE = "-40s%";
    private static String[] analyzeTypes = {
            "PARAMETER",
            "TOTAL SCORE",
            "TOTAL ZEROS WHEN ENEMYS SCORE IS >0",
            "AGREEMENTS NOT FOUND",
            "TOTAL AGREEMENTS WITHOUT ZEROS",
            "TOTAL ZEROS percent"};

    //    easyAnalyzeAndTransform(map);
    public static void main(String[] args) throws IOException {
        File finalLogDirectory = new File(pathToLog());
        Map<String, int[]> sessionsCollection = new HashMap<>();
        for (File file : finalLogDirectory.listFiles()) {
//            file.getPath();
            System.out.println(file.getPath());
            List<LogConverter.Session> sessions = LogConverter.parseFile(file.getPath());
            int[] ints = LogConverter.easyAnalyzeAndTransform(sessions);
            sessionsCollection.put(file.getName(), ints);
            System.out.println("ADDED TO COLLECTION");
        }
        System.out.println("END OF ADDITION");
        printResults(sessionsCollection);
    }

    public static String cutFileName(String name, boolean firstPart) {
        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == '#') {
                if (firstPart) {
                    return name.substring(0, i);
                } else {
                    return name.substring(i + 1);
                }
            }
        }
        return name;
    }

    public static void printResults(Map<String, int[]> resultMap) {
        int i = (resultMap.size() + 1) * COLUMN_WIDTH;
        System.out.println(i);
        System.out.println("Таблица результатов");
        for (int j = 0; j < i; j++) System.out.print("-");
        System.out.println();
//        PrintWriter p = new PrintWriter();
        // System.out.printf("%-40s%-40s%-40s%-40s%n", "botbotbot", "batbatbat", "betbetbet","butbutbut");
        int line = 0;
        while (true) {
            String[] arr = new String[resultMap.keySet().size() + 1];
            arr[0] = analyzeTypes[line];
            int a = 1;
            int linesCount = 0;
            StringBuilder builder = new StringBuilder();
            builder.append(FORMAT_BEGINING);
            builder.append(FORMAT_MIDDLE);
            for (String s : resultMap.keySet()) {
                linesCount = resultMap.get(s).length;
                if (line == 0) {
                    arr[a] = cutFileName(s, true);
                } else {
                    arr[a] = String.valueOf(resultMap.get(s)[line - 1]);
                }
                if (a < resultMap.keySet().size()) {
                    builder.append(FORMAT_MIDDLE);
                }
                a++;
            }
            builder.append(FORMAT_ENDING);
            String string = builder.toString();
            System.out.printf(string, arr);
            line++;
            if (line > linesCount) {
                break;
            }
        }
        for (int j = 0; j < i; j++) System.out.print("-");
    }

    public static String pathToLog() {
        String separator = "/";
        String logs = "/logs";
//        LogConverter.parseFile("D:\\Projects\\bps_insurance\\log-parser\\remoteLog.json");
        System.out.println("ANALYZE DONE");
        String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        System.out.println(path);
        int separatorCounter = 1;
        String relativePath = null;
        for (int i = path.length() - 1; i >= 0; i--) {
            if (path.charAt(i) == separator.charAt(0)) {
                separatorCounter--;
                if (separatorCounter == 0) {
                    relativePath = path.substring(1, i);
                }
            }
        }
        System.out.println();
        String s = relativePath + logs;
        String finalLogDirectoryPath = s.replace("/", "\\");
        System.out.println(finalLogDirectoryPath);
        return finalLogDirectoryPath;
    }
}
